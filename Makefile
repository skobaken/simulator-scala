default: run

build:
	sbt assembly
	cp target/scala-2.10/simulator-assembly-0.1.jar simulator.jar

run: build
	java -jar simulator.jar -r report.txt -i ball.sld -o ball.ppm min-rt.s

debug: build
	java -jar simulator.jar -d -r report.txt -i ball.sld -o ball.ppm min-rt.s

contest: build
	java -jar simulator.jar -r report.txt -i contest.sld -o contest.ppm min-rt-right.s
