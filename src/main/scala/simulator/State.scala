package simulator

import scala.collection.mutable.HashMap
import simulator.instruction.Instruction
import scala.collection.mutable.Stack

class TypeMismatchException extends Exception

case class State(instructions: List[Line]) {
  val MEMORY_SIZE = (1 << 20) - 1
  
  val intregs = new Array[Int](32)
  val floatregs = new Array[Float](32)
  val memory = new Array[Int](MEMORY_SIZE)
  
  val stack = new Stack[(Int, Label)]()
  
  var condreg = State.COND_NO
  var pc = 0
  
  val labelMap = 
    instructions.zipWithIndex.
    collect{ case (l, i) if l.isInstanceOf[Label] => (l.asInstanceOf[Label].getLabel, i) }.
    toMap
    
  val labelCount = HashMap[Label, Long]()
  
  def labelCountInc(label: Label) = {
    labelCount.get(label) match {
      case Some(c) => labelCount.put(label, c+1)
      case None => labelCount.put(label, 1L)
    }
  }
    
  setInt(State.REG_SP, memory.length-1)
  
  def setInt(reg: String, value : Int) = 
    if(reg.substring(0, 2) == "%r"){
      val index = reg.substring(2).toInt
      intregs(index) = value
    } else {
      throw new TypeMismatchException
    }
  
  def setFloat(reg: String, value : Float) = 
    if(reg.substring(0, 2) == "%f"){
      val index = reg.substring(2).toInt
      floatregs(index) = value
    } else {
      throw new TypeMismatchException
    }
  
  def setFloatBits(reg: String, value : Int) = 
    if(reg.substring(0, 2) == "%f"){
      val index = reg.substring(2).toInt
      floatregs(index) = java.lang.Float.intBitsToFloat(value)
    } else {
      throw new TypeMismatchException
    }
  
  def getInt(reg: String) = 
    if(reg.substring(0, 2) == "%r"){
      val index = reg.substring(2).toInt
      intregs(index)
    } else {
      throw new TypeMismatchException
    }
  
  def getFloat(reg: String) = 
    if(reg.substring(0, 2) == "%f"){
      val index = reg.substring(2).toInt
      floatregs(index)
    } else {
      throw new TypeMismatchException
    }
  
  def getFloatBits(reg: String) = 
    if(reg.substring(0, 2) == "%f"){
      val index = reg.substring(2).toInt
      java.lang.Float.floatToIntBits(floatregs(index))
    } else {
      throw new TypeMismatchException
    }
  
  def store(address: Int, value : Int) = memory(address) = value
  
  def store(address: Int, value : Float) = memory(address) = java.lang.Float.floatToIntBits(value)
  
  def loadInt(address: Int) = memory(address)
  
  def loadFloat(address: Int) = java.lang.Float.intBitsToFloat(memory(address))
  
  val opcount = HashMap[Class[_ <: Instruction], Long]()
  
  def opCountInc(c : Class[_ <: Instruction]) : Unit = {
    opcount.get(c) match {
      case Some(count) => opcount.put(c, count+1)
      case None => opcount.put(c,1L)
    }
  }
  
  def printStackTrace = {
    stack.reverse.foreach{ case (num, label) =>
      System.err.println("%d -> %s".format(num, label.getLabel))
    }
  }
  
  def printRegs = {
    def printRegsSub(regs: List[Any], prefix: String) : Unit = {
      for((r,i) <- regs.zipWithIndex){
        System.err.println("%s%d: %s".format(prefix, i, r.toString))
      }
    }
    
    printRegsSub(intregs.toList, "r")
    printRegsSub(floatregs.toList, "f")
  }
}

object State {
  val COND_NO = 0
  val COND_EQ = 1
  val COND_LT = 2
  val COND_GT = 3
  
  val REG_SP = "%r1"
  val REG_LR = "%r0"
}