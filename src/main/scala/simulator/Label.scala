package simulator

case class Label(line : String) extends Line{
  def getLabel = line.split(":")(0)
}