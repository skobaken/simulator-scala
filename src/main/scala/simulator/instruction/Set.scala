package simulator.instruction

import simulator.State

trait Set extends  Instruction {
  private val terms = line.split("\t")(2).split(",")
  val dest = terms(0)
  val s2 = terms(1)
}

object Set{
  def clearLow(i : Int) = {
    i & 0xFFFF0000
  }
  
  def clearHigh(i : Int) = { 
    i & 0x0000FFFF
  }
}

case class Seti(line: String) extends Set {
  def execute(state: State) = {
    state.setInt(dest, s2.toInt)
  }
}

case class SetHi(line: String) extends Set {
  def execute(state: State) = {
    val immValue = try{
      s2.toInt
    } catch {
      case _ : NumberFormatException => state.labelMap(s2)
    }
    val oldValue = state.getInt(dest)
    val newValue = Set.clearLow(immValue) | Set.clearHigh(oldValue)
    state.setInt(dest, newValue)
  }
}

case class SetLo(line: String) extends Set {
  def execute(state: State) = {
    val immValue = try{
      s2.toInt
    } catch {
      case _ : NumberFormatException => state.labelMap(s2)
    }
    val oldValue = state.getInt(dest)
    val newValue = Set.clearHigh(immValue) | Set.clearLow(oldValue)
    state.setInt(dest, newValue)
  }
}

case class SetFHi(line: String) extends Set {
  def execute(state: State) = {
    val imm = s2.toInt
    val oldValue = state.getFloatBits(dest)
    val newValue = (imm << 16) | Set.clearHigh(oldValue)
    state.setFloatBits(dest, newValue)
  }
}

case class SetFLo(line: String) extends Set {
  def execute(state: State) = {
    val imm = s2.toInt
    val oldValue = state.getFloatBits(dest)
    val newValue = Set.clearHigh(imm) | Set.clearLow(oldValue)
    state.setFloatBits(dest, newValue)
  }
}