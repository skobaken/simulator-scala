package simulator.instruction

import simulator.State

trait Shift extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val dest = terms(0)
  val s1 = terms(1)
  val imm = terms(2).toInt
}

case class SRAi(line: String) extends Shift {
  def execute(state:State) = {
    state.setInt(dest, state.getInt(s1) >> imm)
  }
}

case class SRLi(line: String) extends Shift {
  def execute(state:State) = {
    state.setInt(dest, state.getInt(s1) >>> imm)
  }
}

case class SLLi(line: String) extends Shift {
  def execute(state:State) = {
    state.setInt(dest, state.getInt(s1) << imm)
  }
}