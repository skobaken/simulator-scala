package simulator.instruction

import simulator.State

trait MonoOp extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val dest = terms(0)
  val s = terms(1)
  
  def execute(state: State) = state.setFloat(dest, op(state.getFloat(s)))

  val op : Float => Float
}

case class FMv(line:String) extends MonoOp{
  val op  = {a : Float => a}
}

case class FSqrt(line:String) extends MonoOp{
  val op  = {a : Float => Math.sqrt(a).toFloat}
}

case class FInv(line:String) extends MonoOp{
  val op  = {a : Float => (1.0 / a).toFloat}
}

case class FNeg(line:String) extends MonoOp{
  val op  = {a : Float => -a}
}

case class FAbs(line:String) extends MonoOp{
  val op  = {a : Float => Math.abs(a)}
}

case class Floor(line:String) extends MonoOp{
  val op  = {a : Float => Math.floor(a).toFloat}
}