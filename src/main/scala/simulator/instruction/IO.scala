package simulator.instruction

import simulator.State
import simulator.StdIO

trait IO extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val reg = terms(0)
}

case class Output(line: String) extends IO{
  def execute(state: State) = {
    StdIO.printInt(state.getInt(reg))
  }
}

case class Outputb(line: String) extends IO{
  def execute(state: State) = {
    StdIO.printByte((0x000000FF & state.getInt(reg)).asInstanceOf[Byte])
  }
}

case class Outputf(line: String) extends IO{
  def execute(state: State) = {
    StdIO.printFloat(state.getFloat(reg))
  }
}

case class Input(line: String) extends IO{
  def execute(state: State) = {
    state.setInt(reg, StdIO.readInt)
  }
}

case class Inputb(line: String) extends IO{
  def execute(state: State) = {
    state.setInt(reg, StdIO.readByte)
  }
}

case class Inputf(line: String) extends IO{
  def execute(state: State) = {
    state.setFloat(reg, StdIO.readFloat)
  }
}