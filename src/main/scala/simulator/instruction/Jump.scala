package simulator.instruction

import simulator.State
import simulator.State._
import simulator.Label

trait Jump extends Instruction {
  private val terms = line.split("\t").toList.tail.tail.headOption
  val dest = terms.getOrElse("")
  
  def before(staete: State) = {}
  
  def after(state: State) = {}
  
  def shouldJump(state: State) : Boolean
  
  val shouldStack = false
  
  def execute(state: State) = {
    if(shouldJump(state)){
      before(state)
      val next =dest match {
        case a if state.labelMap.contains(a) => {
          //System.err.println(a)
          state.labelMap(a)
        }
        case a => state.getInt(a)
      }
      
      state.instructions(next) match {
        case label : Label => {
          state.labelCountInc(label)
          
          if(shouldStack){
            state.stack.push((state.pc, label))
          }
        }
        case _ => throw new Exception("jump to not label");
      }
      
      state.pc = next
      
      after(state)
    }
  }
}

case class BEQ(line: String) extends Jump{
  def shouldJump(state: State) = (state.condreg == COND_EQ)
}

case class BLT(line: String) extends Jump{
  def shouldJump(state: State) = (state.condreg == COND_LT)
}

case class BGT(line: String) extends Jump{
  def shouldJump(state: State) = (state.condreg == COND_GT)
}

case class J(line: String) extends Jump{
  def shouldJump(state: State) = (true)
}

case class JR(line: String) extends Jump{
  def shouldJump(state: State) = (true)
}

case class Call(line: String) extends Jump{
  override def before(state: State) = {
    state.store(state.getInt(REG_SP), state.getInt(REG_LR))
    state.setInt(REG_SP, state.getInt(REG_SP)-1)
    state.setInt(REG_LR, state.pc)
  }
  
  def shouldJump(state: State) = true
  
  override val shouldStack = true
}

case class CallR(line: String) extends Jump{
  override def before(state: State) = {
    state.store(state.getInt(REG_SP), state.getInt(REG_LR))
    state.setInt(REG_SP, state.getInt(REG_SP)-1)
    state.setInt(REG_LR, state.pc)
  }
  
  def shouldJump(state: State) = true
  
  override val shouldStack = true
}

case class Return(line: String) extends Instruction{
  def execute(state: State) = {
    state.setInt(REG_SP, state.getInt(REG_SP)+1)
        
    state.pc = state.getInt(REG_LR)
    
    state.setInt(REG_LR, state.loadInt(state.getInt(REG_SP)))
    
    state.stack.pop
  }
}