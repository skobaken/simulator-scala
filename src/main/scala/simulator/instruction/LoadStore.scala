package simulator.instruction

import simulator.State

trait LoadInt extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val dest = terms(0)
  val s1 = terms(1)
  val s2 = terms(2)
  
  def address(state: State) : Int
  
  def execute(state: State) = {
    state.setInt(dest, state.loadInt(address(state)))
  }
}

case class Ld(line: String) extends LoadInt {
  def address(state: State) = state.getInt(s1) + state.getInt(s2)
}

case class Ldi(line: String) extends LoadInt {
  def address(state: State) = state.getInt(s1) + s2.toInt
}

trait LoadFloat extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val dest = terms(0)
  val s1 = terms(1)
  val s2 = terms(2)
  
  def address(state: State) : Int
  
  def execute(state: State) = {
    state.setFloat(dest, state.loadFloat(address(state)))
  }
}

case class Ldf(line: String) extends LoadFloat {
  def address(state: State) = state.getInt(s1) + state.getInt(s2)
}

case class Ldfi(line: String) extends LoadFloat {
  def address(state: State) = state.getInt(s1) + s2.toInt
}

trait StoreInt extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val t1 = terms(0)
  val t2 = terms(1)
  val t3 = terms(2)
  
  def address(state: State) : Int
  
  def src : String
  
  def execute(state: State) = {
    state.store(address(state), state.getInt(src))
  }
}

case class St(line: String) extends StoreInt {
  def address(state: State) = state.getInt(t1) + state.getInt(t2)
  
  def src = t3
}

case class Sti(line: String) extends StoreInt {
  def address(state: State) = state.getInt(t1) + t3.toInt
  
  def src = t2
}

trait StoreFloat extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val t1 = terms(0)
  val t2 = terms(1)
  val t3 = terms(2)
  
  def address(state: State) : Int
  
  def src : String
  
  def execute(state: State) = {
    state.store(address(state), state.getFloat(src))
  }
}

case class Stf(line: String) extends StoreFloat {
  def address(state: State) = state.getInt(t1) + state.getInt(t2)
  
  def src = t3
}

case class Stfi(line: String) extends StoreFloat {
  def address(state: State) = state.getInt(t1) + t3.toInt
  
  def src = t2
}
