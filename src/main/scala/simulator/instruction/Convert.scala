package simulator.instruction

import simulator.State

trait Convert extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val dest = terms(0)
  val src = terms(1)
}

case class IoF(line: String) extends Convert {
  def execute(state:State) = {
    state.setInt(dest, Math.round(state.getFloat(src)))
  }
}

case class FoI(line: String) extends Convert {
  def execute(state:State) = {
    state.setFloat(dest, state.getInt(src).asInstanceOf[Float])
  }
}
