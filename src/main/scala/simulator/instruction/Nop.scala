package simulator.instruction

import simulator.State

case class Nop(line : String) extends Instruction{
  def execute(state: State) = ()
}