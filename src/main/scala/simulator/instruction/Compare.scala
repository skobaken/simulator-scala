package simulator.instruction

import simulator.State

trait Compare extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val s1 = terms(0)
  val s2 = terms(1)
}

case class Cmp(line: String) extends Compare {
  def execute(state: State)=  {
    val cond = state.getInt(s1).compare(state.getInt(s2)) match {
      case a if a < 0 => State.COND_LT
      case a if a > 0 => State.COND_GT
      case 0 => State.COND_EQ
    }
    
    state.condreg = cond
  }
}

case class Cmpi(line: String) extends Compare {
  def execute(state: State)=  {
    val cond = state.getInt(s1).compare(s2.toInt) match {
      case a if a < 0 => State.COND_LT
      case a if a > 0 => State.COND_GT
      case 0 => State.COND_EQ
    }
    
    state.condreg = cond
  }
}

case class Cmpf(line: String) extends Compare {
  def execute(state: State)=  {
    val cond = state.getFloat(s1).compare(state.getFloat(s2)) match {
      case a if a < 0 => State.COND_LT
      case a if a > 0 => State.COND_GT
      case 0 => State.COND_EQ
    }
    
    state.condreg = cond
  }
}