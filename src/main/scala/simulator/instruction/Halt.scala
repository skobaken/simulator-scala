package simulator.instruction

import simulator.State

case class Halt(line : String) extends Instruction{
  def execute(state: State) = state.pc = Int.MaxValue
}