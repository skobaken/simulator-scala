package simulator.instruction

import simulator.Line
import simulator.State
import scala.util.matching.Regex
import scala.collection.immutable.HashSet
import simulator.ParseException
import scala.collection.mutable.HashMap

trait Instruction extends Line{
  def exec(state: State) = {
    try{
      state.opCountInc(getClass())
      execute(state)
    } catch {
      case e : Throwable=> {
        state.printRegs
        System.err.println("**********************")
        state.printStackTrace
        System.err.println("**********************")
        System.err.println("%d: %s".format(state.pc + 1, line))
        throw e
      }
    }
  }
  
  def execute(state: State) : Unit
}

object Instruction{
  val instructions  : HashSet[Class[_ <: Instruction]]= HashSet(
    classOf[Add],
    classOf[Sub],
    classOf[Mul],
    classOf[Div],
    classOf[Addi],
    classOf[Subi],
    classOf[Muli],
    classOf[Divi],
    classOf[FAdd],
    classOf[FSub],
    classOf[FMul],
    classOf[FDiv],
    classOf[Cmp],
    classOf[Cmpi],
    classOf[Cmpf],
    classOf[IoF],
    classOf[FoI],
    classOf[Halt],
    classOf[Input],
    classOf[Inputb],
    classOf[Inputf],
    classOf[Output],
    classOf[Outputb],
    classOf[Outputf],
    classOf[BEQ],
    classOf[BLT],
    classOf[BGT],
    classOf[Call],
    classOf[CallR],
    classOf[Return],
    classOf[J],
    classOf[JR],
    classOf[Ld],
    classOf[Ldi],
    classOf[Ldf],
    classOf[Ldfi],
    classOf[St],
    classOf[Sti],
    classOf[Stf],
    classOf[Stfi],
    classOf[FMv],
    classOf[FInv],
    classOf[FSqrt],
    classOf[Floor],
    classOf[FNeg],
    classOf[FAbs],
    classOf[Nop],
    classOf[Seti],
    classOf[SetHi],
    classOf[SetLo],
    classOf[SetFHi],
    classOf[SetFLo],
    classOf[SRAi],
    classOf[SRLi],
    classOf[SLLi]
  )
  
  def decode(str : String) = {
    val name = str.split("\t")(1)
    val insts = instructions.filter(c => c.getSimpleName.toLowerCase == name)
    val tmp = instructions.head.getSimpleName
    
    insts.size match {
      case 1 => {
        val inst = insts.head
        val constructor = inst.getConstructor(classOf[String])
        constructor.newInstance(str)
      }

      case _ => throw new ParseException("invalid op: %s".format(str))
    }
  }
}
