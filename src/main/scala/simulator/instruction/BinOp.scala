package simulator.instruction

import simulator.State

trait IntBinOp extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val dest = terms(0)
  val s1 = terms(1)
  val s2 = terms(2)
  
  
  def execute(state: State) = 
    try {
  	  val imm = s2.toInt
  	  state.setInt(dest, op(state.getInt(s1), imm))
    } catch {
      case _ : NumberFormatException => state.setInt(dest, op(state.getInt(s1), state.getInt(s2)))
    }
  
  val op : (Int, Int) => Int
}

trait FloatBinOp extends Instruction {
  private val terms = line.split("\t")(2).split(",")
  val dest = terms(0)
  val s1 = terms(1)
  val s2 = terms(2)
  
  def execute(state: State) = state.setFloat(dest, op(state.getFloat(s1), state.getFloat(s2)))

  val op : (Float, Float) => Float
}

case class Add(line: String) extends IntBinOp {
  val op = (a: Int,b: Int) => a+b
}

case class Sub(line: String) extends IntBinOp {
  val op = (a: Int,b: Int) => a-b
}

case class Mul(line: String) extends IntBinOp {
  val op = (a: Int,b: Int) => a*b
}

case class Div(line: String) extends IntBinOp {
  val op = (a: Int,b: Int) => a/b
}

case class Addi(line: String) extends IntBinOp {
  val op = (a: Int,b: Int) => a+b
}

case class Subi(line: String) extends IntBinOp {
  val op = (a: Int,b: Int) => a-b
}

case class Muli(line: String) extends IntBinOp {
  val op = (a: Int,b: Int) => a*b
}

case class Divi(line: String) extends IntBinOp {
  val op = (a: Int,b: Int) => a/b
}

case class FAdd(line: String) extends FloatBinOp {
  val op = (a: Float,b: Float) => a+b
}

case class FSub(line: String) extends FloatBinOp {
  val op = (a: Float,b: Float) => a-b
}

case class FMul(line: String) extends FloatBinOp {
  val op = (a: Float,b: Float) => a*b
}

case class FDiv(line: String) extends FloatBinOp {
  val op = (a: Float,b: Float) => a/b
}