package simulator

import java.io.BufferedInputStream
import java.io.FileReader
import scala.collection.mutable.ListBuffer
import java.io.BufferedReader
import simulator.instruction.Instruction
import org.gnu.readline.Readline
import org.gnu.readline.ReadlineLibrary

object Simulator {
  val ENTRY_POINT = "@_min_caml_start"
    
  var is_debug : Boolean = false
  
  def main(argsArray: Array[String]): Unit = {
    var args = argsArray.toList
    
    if(args.size == 0){
      System.err.println("input args")
      args = readLine.split(" ").toList.filter(_ != "")
    }
    
    while(args.head.startsWith("-")){
      args.head match {
        case "-i" => {
          args = args.tail
          StdIO.setIn(args.head)
        }
        
        case "-o" => {
          args = args.tail
          StdIO.setOut(args.head)
        }
        
        case "-d" => {
          is_debug = true
        }
        
        case "-r" => {
          args = args.tail
          StdIO.setReport(args.head)
        }
       
        case _ => ()
      }
      
      args = args.tail
    }
    
    System.err.println("parsing files")
    val instructions = Parser.parseAll(args)
    
    val state = State(instructions)
    
    state.pc = state.labelMap(ENTRY_POINT)
    
    
    if(is_debug){
      debug(state)
    } else {
      System.err.println("simulation start")
      run(state)
    }
      
    StdIO.printReport("*********inst**********")
    writeInstCount(state)
    StdIO.printReport("*********label*********")
    writeLabelCount(state)
  }
  
  def writeInstCount(state: State) = {
    var sum = 0L
    for((c, count) <- state.opcount){
      StdIO.printReport("%s: %d".format(c.getSimpleName().toLowerCase(), count))
      sum += count
    }
    
    StdIO.printReport("all: %d".format(sum))
  }
  
  def writeLabelCount(state: State) = {
    for((label, count) <- state.labelCount){
      StdIO.printReport("%s: %d".format(label.getLabel, count))
    }
  }
  
  def run(state : State) : Unit = {
    val cur = state.instructions(state.pc)
    
    cur match {
      case inst: Instruction => 
        inst.exec(state)
      case label: Label =>
        state.labelCountInc(label)
       
      case _ => ()
    }
    
    if(state.pc < Int.MaxValue){
      state.pc += 1
      run(state)
    }
  }
  
  def debug(state: State) : Unit = {
    def debugRun(state: State, num : Option[Int], isFirst: Boolean) : Unit= {
      def extraCond(state: State) = true
      
      if(num.isDefined && num.get <= 0){
        return
      }
      
      val cur = state.instructions(state.pc)
      
      if(cur.break && !isFirst && extraCond(state)){
        return
      }
    
      cur match {
        case inst: Instruction => 
          inst.exec(state)
        case _ => ()
      }
    
      if(state.pc < Int.MaxValue){
        state.pc += 1
        debugRun(state, num.map(_-1), false)
      }
    }
    
    Readline.load(ReadlineLibrary.GnuReadline);
    Readline.initReadline("simulator-scala");
    
    var before = "s"
    while(true){
      println("%d: %s".format(state.pc+1, state.instructions(state.pc).toString))
      
      val command = Some(Readline.readline("command> ")).getOrElse(before)
      before = command
    
      command.charAt(0) match {
        case 'r' =>{
          val limit = try{
            Some(command.stripLineEnd.substring(1).toInt)
          } catch {
            case _ : Throwable => None
          }

          debugRun(state, limit, true)
        }
        
        case 'b' => {
          val linenum = command.stripLineEnd.substring(1).toInt-1
          state.instructions(linenum).break = true
        }
        
        case 'i' => {
          val linenum = try{
            command.stripLineEnd.substring(1).toInt-1
          } catch {
            case _ : Throwable => state.pc
          }
          
          println("%d: %s".format(linenum+1, state.instructions(linenum).toString))
        }
      
        case 's' => {
          val count = try{
            command.stripLineEnd.substring(1).toInt
          } catch {
            case _ : Throwable => 1
          }
          
          debugRun(state, Some(count), true)
        }
      
        case 'p' => {
          state.printRegs
        }
        
        case 'm' => {
          val address = command.stripLineEnd.substring(1).toInt
          println("%d: %d %f".format(address, state.loadInt(address), state.loadFloat(address)))
        }
      
        case 'e' => {
          return
        }
      }
    }
  }
}