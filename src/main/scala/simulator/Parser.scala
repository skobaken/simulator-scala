package simulator

import java.io.BufferedReader
import scala.collection.mutable.ListBuffer
import java.io.FileReader
import java.io.Reader
import scala.io.Source
import java.io.InputStreamReader

object Parser {
  def parseAll(files : List[String]) : List[Line] = (files.toList.map(parse) :+ parseLib).flatten
   
  def parseLib : List[Line] = {
    val is = getClass.getResourceAsStream("libmincaml.s")
    
    parse(new InputStreamReader(is))
  }
  
  def parse(file: String) : List[Line] = parse(new FileReader(file))
  
  def parse(reader: Reader) :List[Line] = {
    val fr = new BufferedReader(reader)
    
    var str = fr.readLine
    val lines = new ListBuffer[Line]
    while(str != null && !str.isEmpty()){
      lines += Line.decode(str.stripLineEnd)
      str = fr.readLine
    }
    
    return lines.toList
  }
}