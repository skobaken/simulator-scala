package simulator

import simulator.instruction.Instruction

trait Line{
  val line: String
  
  var break = false
}

object Line {
  def decode(str : String) :Line = {
    try{
      str.charAt(0) match {
        case '@' => new Label(str)
        case '#' => new Comment(str)
        case '\t' => {
          str.charAt(1) match {
            case '.' => new Comment(str)
            case _ => Instruction.decode(str)
          }
        }
        case _ => throw new ParseException(str)
      }
    } catch {
      case e: Throwable => {
        System.err.println(str)
        throw e
      }
    }
  }
}