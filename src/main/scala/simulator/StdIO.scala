package simulator

import java.util.Scanner
import java.io.File
import java.io.OutputStream
import java.io.FileOutputStream
import java.io.PrintStream

object StdIO {
  var scanner : Scanner = null
  var output : PrintStream = System.out
  var report : PrintStream = null
  
  def setIn(file: String) = {
    if(file != null && !file.isEmpty()){
      try{
        scanner = new Scanner(new File(file))
      } catch {
        case _ : Throwable => System.err.println("not found file")
      }
    }
  }
  
  def setOut(file: String) = {
    if(file != null && !file.isEmpty()){
      output = new PrintStream(new FileOutputStream(new File(file)))
    }
  }
  
  def setReport(file: String) = {
    if(file != null && !file.isEmpty()){
      report = new PrintStream(new FileOutputStream(new File(file)))
    }
  }
  
  def readInt = scanner.nextInt
  def readFloat = scanner.nextFloat  
  def readByte = scanner.nextByte
  
  def printInt(a: Int) = output.print(a)
  def printFloat(a: Float) = output.print(a)
  def printByte(a: Byte) = output.write(a)

  def printReport(line : Any) = if(report != null) report.println(line) else ()
}