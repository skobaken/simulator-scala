	.globl	@min_caml_div
@min_caml_div:
	addi	%r4,%r2,0
	seti	%r2,0
@div_cont:
	cmp	%r4,%r3
	blt	@div_return
	addi	%r2,%r2,1
	sub	%r4,%r4,%r3
	j	@div_cont
@div_return:
	return
	.globl	@min_caml_mod
@min_caml_mod:
	cmp	%r3,%r2
	bgt	@mod_return
	sub	%r2,%r2,%r3
	j	@min_caml_mod
@mod_return:
	return
	.globl	@min_caml_print_newline
@min_caml_print_newline:
	seti	%r2,10
	outputb	%r2
	return
	.globl	@min_caml_print_char
@min_caml_print_char:
	outputb	%r2
	return
	.globl	@min_caml_print_int
@min_caml_print_int:
	output	%r2
	return
	.globl	@min_caml_print_byte
@min_caml_print_byte:
	outputb	%r2
	return
	.globl	@min_caml_print_float
@min_caml_print_float:
	outputf	%f6
	return
	.globl	@min_caml_prerr_int
@min_caml_prerr_int:
	output	%r2
	return
	.globl	@min_caml_prerr_byte
@min_caml_prerr_byte:
	outputb	%r2
	return
	.globl	@min_caml_prerr_float
@min_caml_prerr_float:
	outputf	%f6
	return
	.globl	@min_caml_read_int
@min_caml_read_int:
	input	%r2
	return
	.globl	@min_caml_read_byte
@min_caml_read_byte:
	inputb	%r2
	return
	.globl	@min_caml_read_float
@min_caml_read_float:
	inputf	%f6
	return
	.globl	@min_caml_create_array
@min_caml_create_array:
	addi	%r4,%r2,0
	addi	%r2,%r31,0
@create_array_loop:
	cmpi	%r4,0
	beq	@create_array_return
	sti	%r31,%r3,0
	addi	%r4,%r4,-1
	addi	%r31,%r31,1
	j	@create_array_loop
@create_array_return:
	return
	.globl	@min_caml_create_float_array
@min_caml_create_float_array:
	addi	%r4,%r2,0
	addi	%r2,%r31,0
@create_float_array_loop:
	cmpi	%r4,0
	beq	@create_float_array_return
	stfi	%r31,%f6,0
	addi	%r4,%r4,-1
	addi	%r31,%r31,1
	j	@create_float_array_loop
@create_float_array_return
	return
	.globl	@min_caml_abs_float
@min_caml_abs_float:
	.globl	@min_caml_fabs
@min_caml_fabs:
	fabs	%f6,%f6
	return
	.globl	@min_caml_sqrt
@min_caml_sqrt:
	fsqrt	%f6,%f6
	return
	.globl	@min_caml_truncate
@min_caml_truncate:
	.globl	@min_caml_int_of_float
@min_caml_int_of_float:
	iof	%r2,%f6
	return
	.globl	@min_caml_floor
@min_caml_floor:
	floor	%f6,%f6
	return
	.globl	@min_caml_float_of_int
@min_caml_float_of_int:
	foi	%f6,%r2
	return
	.globl	@min_caml_cos
@min_caml_cos:
	seti	%r2,1
	setfhi	%f7,16256
	setflo	%f7,0
	setfhi	%f8,16256
	setflo	%f8,0
	seti	%r3,20
@cos_sub:
	cmp	%r2,%r3
	blt	@cos_sub_cont
	fmv	%f6,%f8
	return
@cos_sub_cont:
	setfhi	%f9,49024
	setflo	%f9,0
	fmul	%f9,%f9,%f6
	fmul	%f9,%f9,%f6
	fmul	%f7,%f9,%f7
	muli	%r4,%r2,2
	muli	%r5,%r2,2
	addi	%r5,%r5,-1
	mul	%r4,%r4,%r5
	foi	%f10,%r4
	finv	%f10,%f10
	fmul	%f7,%f7,%f10
	addi	%r2,%r2,1
	fadd	%f8,%f8,%f7
	j	@cos_sub
	.globl	@min_caml_sin
@min_caml_sin:
	seti	%r2,1
	fmv	%f7,%f6
	fmv	%f8,%f6
	seti	%r3,20
@sin_sub:
	cmp	%r2,%r3
	blt	@sin_sub_cont
	fmv	%f6,%f8
	return
@sin_sub_cont:
	setfhi	%f9,49024
	setflo	%f9,0
	fmul	%f9,%f9,%f6
	fmul	%f9,%f9,%f6
	fmul	%f7,%f9,%f7
	muli	%r4,%r2,2
	muli	%r5,%r2,2
	addi	%r5,%r5,1
	mul	%r4,%r4,%r5
	foi	%f10,%r4
	finv	%f10,%f10
	fmul	%f7,%f7,%f10
	addi	%r2,%r2,1
	fadd	%f8,%f8,%f7
	j	@sin_sub
	.globl	@min_caml_atan
@min_caml_atan:
	seti	%r2,1
	fmv	%f7,%f6
	fmv	%f8,%f6
	seti	%r3,50
@atan_sub:
	cmp	%r2,%r3
	blt	@atan_sub_cont
	fmv	%f6,%f8
	return
@atan_sub_cont:
	setfhi	%f9,49024
	setflo	%f9,0
	fmul	%f9,%f9,%f6
	fmul	%f9,%f9,%f6
	fmul	%f7,%f9,%f7
	muli	%r6,%r2,2
	addi	%r7,%r6,-1
	foi	%f9,%r7
	fmul	%f7,%f7,%f9
	addi	%r7,%r6,1
	foi	%f9,%r7
	finv	%f9,%f9
	fmul	%f7,%f7,%f9
	addi	%r2,%r2,1
	fadd	%f8,%f8,%f7
	j	@atan_sub
	.globl	@min_caml_fispos
@min_caml_fispos:
	setfhi	%f7,0
	setflo	%f7,0
	cmpf	%f7,%f6
	blt	@fispos_blt
	seti	%r2,0
	return
@fispos_blt:
	seti	%r2,1
	return
	.globl	@min_caml_fisneg
@min_caml_fisneg:
	setfhi	%f7,0
	setflo	%f7,0
	cmpf	%f6,%f7
	blt	@fisneg_blt
	seti	%r2,0
	return
@fisneg_blt:
	seti	%r2,1
	return
	.globl	@min_caml_fiszero
@min_caml_fiszero:
	setfhi	%f7,0
	setflo	%f7,0
	cmpf	%f6,%f7
	beq	@fiszero_beq
	seti	%r2,0
	return
@fiszero_beq:
	seti	%r2,1
	return
	.globl	@min_caml_fhalf
@min_caml_fhalf:
	setfhi	%f7,16128
	setflo	%f7,0
	fmul	%f6,%f6,%f7
	return
	.globl	@min_caml_fsqr
@min_caml_fsqr:
	fmul	%f6,%f6,%f6
	return
	.globl	@min_caml_fneg
@min_caml_fneg:
	fneg	%f6,%f6,%f6
	return
	.globl	@min_caml_fless
@min_caml_fless:
	cmpf	%f6,%f7
	blt	@fless_blt
	seti	%r2,0
	return
@fless_blt:
	seti	%r2,1
	return

