import AssemblyKeys._

name := "simulator"
 
version := "0.1"
 
organization := "jp.skobaken"
 
scalaVersion := "2.10.3"
 
//libraryDependencies += "org.apache.commons" % "commons-math3" % "3.0"
libraryDependencies ++= Seq(
)
 
javacOptions ++= Seq("-source", "1.7", "-target", "1.7", "-encoding", "UTF-8")
 
scalacOptions ++= Seq("-deprecation", "-encoding", "UTF-8")
 
mainClass in Compile := Some("simulator.Simulator")

assemblySettings
